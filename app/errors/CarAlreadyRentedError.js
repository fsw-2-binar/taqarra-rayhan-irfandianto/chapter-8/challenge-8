const ApplicationError = require("./ApplicationError");
const {car}= require('../models')
class CarAlreadyRentedError extends ApplicationError {
  constructor(car) {
    super(`${car.name} is already rented!!`);
  }

  get details() {
    return { car }
  }
}

module.exports = CarAlreadyRentedError;
