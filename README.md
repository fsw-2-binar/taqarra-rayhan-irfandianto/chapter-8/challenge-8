# BCR API
Heroku Link
https://dev-db-challenge8-rayhan.herokuapp.com/

![coverage terakhir](https://gitlab.com/fsw-2-binar/taqarra-rayhan-irfandianto/chapter-8/challenge-8/-/raw/main/Screenshot%202022-06-10%20201941.png)

Di dalam repository ini terdapat implementasi API dari Binar Car Rental.
Tugas kalian disini adalah:
1. Fork repository
2. Tulis unit test di dalam repository ini menggunakan `jest`.
3. Coverage minimal 70%

Good luck!
