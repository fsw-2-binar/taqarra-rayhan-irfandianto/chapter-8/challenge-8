require('dotenv').config()
const request = require("supertest");
const app = require("../../app");
jest.setTimeout(50000)
describe("GET /v1/cars/", () => {
   
    it("should response with 200 as status code, get all car", async () => {

        return request(app)
          .get("/v1/cars")
          .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual(res.body);
          });
      });
      it("should response with 200 as status code, get car by id", async () => {
        let id = 20

        return request(app)
          .get(`/v1/cars/${id}`)
          .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual(res.body);
          });
      });
    
      it("should response with 201 as status code,create", async () => {
        const name = "cadilac";
        const price = 120000;
        const image="https://image.shutterstock.com/z/stock-photo-st-petersburg-russia-september-cadillac-escalade-iv-esv-depp-limousine-front-three-2041987862.jpg";
        const size="MEDIUM";
        const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwibmFtZSI6InRhcWFycmExIiwiZW1haWwiOiJ0YXFAZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ3NzUxMzd9.sotverQ8XB-_NJ1ZXxEOBV6CaeLlbn9oZozek307LLQ"

        let data = {
            name : name,
            price : price,
            image : image,
            size : size
        }
    
        return request(app)
          .post("/v1/cars")
          .set("Content-Type", "application/json")
          .set("Authorization", `Bearer ${token}`)
          .send(data)
          .then((res) => {
            expect(res.statusCode).toBe(201);
            expect(res.body).toEqual(res.body);
            
          });
        });


        it("should response with 204 as status code,delete car", async () => {
          let idMobil = 16
          let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwibmFtZSI6InRhcWFycmExIiwiZW1haWwiOiJ0YXFAZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ3NzUxMzd9.sotverQ8XB-_NJ1ZXxEOBV6CaeLlbn9oZozek307LLQ"
          
          return request(app)
              .delete(`/v1/cars/${idMobil}`)
              .set("Authorization", `Bearer ${token}`)
              .then((res) => {
              expect(res.statusCode).toBe(204);
              expect(res.body).toEqual(res.body);
              });
          });  

          it("should response with 200 as status code,update", async () => {
            const name = "Brun";
            const price = 1900000;
            const image="https://image.shutterstock.com/z/stock-photo-st-petersburg-russia-september-cadillac-escalade-iv-esv-depp-limousine-front-three-2041987862.jpg";
            const size="SMALL";
            const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwibmFtZSI6InRhcWFycmExIiwiZW1haWwiOiJ0YXFAZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ3NzUxMzd9.sotverQ8XB-_NJ1ZXxEOBV6CaeLlbn9oZozek307LLQ"
            let idMobil = 48
        
            let data = {
                name : name,
                price : price,
                image : image,
                size : size
            }
            
            return request(app)
                .put(`/v1/cars/${idMobil}`)
                .set("Authorization", `Bearer ${token}`)
                .send(data)
                .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual(res.body);
                });
            });
    
    
            it("should response with 201 as status code, rent car", async () => {
            
              let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwibmFtZSI6ImphbnNlbiIsImVtYWlsIjoiamFuc2VuQG1haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3MTA4MzF9.bFUqS5glwJHqjRvZBq_gB0MVuuLnIMDe6uvLSDVc2_Q"
              
              return request(app)
                .post(`/v1/cars/48/rent`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${token}`)
                .send({
                  'rentStartedAt': "2022-06-08T15:23:41.438Z",
                  'rentEndedAt': "2022-06-25T15:23:41.438Z"
                })
                .then((res) => {
                  expect(res.statusCode).toBe(201);
                  expect(res.body).toEqual(res.body);
                });
              });
  
  

});
