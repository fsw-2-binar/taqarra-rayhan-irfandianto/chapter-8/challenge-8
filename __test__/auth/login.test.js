require('dotenv').config()
const request = require("supertest");
const app = require("../../app");
jest.setTimeout(50000)
describe("POST /v1/auth/login", () => {
    // login 
    it("should response with 201 as status code", async () => {
        const email = "fikri@binar.co.id";
        const password = "123456";
        let data = {
            email: email,
            password: password
        }

        return request(app)
            .post("/v1/auth/login")
            .set("Content-Type", "application/json")
            .send(data)
            .then((res) => {
                expect(res.statusCode).toBe(201);
                expect(res.body).toEqual(res.body);
            });
    });

    
    
    
    
    
    
    

    // Test jika error email gada
    it("should response with 404 as status code", async () => {
        const email = "atest123@binar.co.id";
        const password = "a1234";
        let data = {
            email: email,
            password: password
        }

        return request(app)
            .post("/v1/auth/login")
            .set("Content-Type", "application/json")
            .send(data)
            .then((res) => {
                expect(res.statusCode).toBe(404);
                expect(res.body).toEqual(res.body);
            });
    });

    // Test email kosong
    it("should response with 401 as status code", async () => {
        const email = "fikri@binar.co.id";
        const password = "";
        let data = {
            email: email,
            password: password
        }

        return request(app)
            .post("/v1/auth/login")
            .set("Content-Type", "application/json")
            .send(data)
            .then((res) => {
                expect(res.statusCode).toBe(401);
                expect(res.body).toEqual(res.body);
            });
    });
    it("should response with 200 as status code", async () => {

        let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwibmFtZSI6InRhcWFycmExIiwiZW1haWwiOiJ0YXFAZ21haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3NzM5MTJ9.GLwIxhGcepYqVP4ayLmhhbdd7cl0qPPgXelpMtaY2sw"
        
        return request(app)
          .get("/v1/Auth/whoami") 
          .set("Content-Type", "application/json")
          .set("Authorization", `Bearer ${token}`)
          .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual(res.body);
          });
      });
    
});
