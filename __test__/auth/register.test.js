require('dotenv').config()
const request = require("supertest");
const app = require("../../app");

jest.setTimeout(50000)
describe("POST /v1/auth/register", () => {
    
    it("should response with 201 as status code register harusnya berhasil", async () => {
        const name = "irfan"
        const email = "irfan125@gmail.com"
        const password = "123456"
        let data = {
            name: name,
            email: email,
            password: password
        }

        return request(app)
            .post("/v1/auth/register")
            .set("Content-Type", "application/json")
            .send(data)
            .then((res) => {
                expect(res.statusCode).toBe(201);
                expect(res.body).toEqual(res.body);
            });
    });



    it("should response with 422 test jika email sama", async () => {
        const email = "johnny@binar.co.id";
        const password = "123456";
        
        let data = {
          
          email: email,
          password: password
        }
    
        return request(app)
          .post("/v1/auth/register")
          .set("Content-Type", "application/json")
          .send(data)
          .then((res) => {
            expect(res.statusCode).toBe(422);
            expect(res.body).toEqual(res.body);
            
          });
      });

})